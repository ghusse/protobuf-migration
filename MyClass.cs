﻿namespace Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Runtime.Serialization;

	[DataContract]
	public class MyClass
	{
		public MyClass()
		{
			this.ObjectValue = new ParentValue();
		}

		[DataMember(Order = 1)]
		public ParentValue ObjectValue { get; set; }
	}
}
