﻿namespace ModelTest
{
	using System;
	using System.Text;
	using System.Collections.Generic;
	using System.Linq;
	using Microsoft.VisualStudio.TestTools.UnitTesting;
	using Model;
	using System.IO;
	using ProtoBuf;

	[TestClass]
	public class SerializationTest
	{
		[TestMethod]
		public void CloneWithDifferentObjectValue()
		{
			MyClass foo = new MyClass();
			foo.ObjectValue = new ChildValue
			{
				Value = 42
			};

			MyClass clone = Clone(foo);

			Assert.AreEqual(42, (clone.ObjectValue as ChildValue).Value);
		}


		private static MyClass Clone(MyClass foo)
		{
			MyClass clone;

			using (MemoryStream stream = new MemoryStream())
			{
				Serializer.Serialize<MyClass>(stream, foo);
				stream.Seek(0, SeekOrigin.Begin);

				clone = ProtoBuf.Serializer.Deserialize<MyClass>(stream);
			}

			return clone;
		}


	}
}
