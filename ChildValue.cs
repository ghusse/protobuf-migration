﻿namespace Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Runtime.Serialization;

	[DataContract]
	public class ChildValue : ParentValue
	{
		[DataMember(Order = 1)]
		public int Value { get; set; }
	}
}
