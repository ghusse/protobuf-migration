﻿namespace Model
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Text;
	using System.Runtime.Serialization;
	using ProtoBuf;

	[DataContract]
	[ProtoInclude(100, typeof(ChildValue))]
	public class ParentValue
	{
	}
}
